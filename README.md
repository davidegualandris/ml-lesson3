# [ML] Lesson3

These are the Jupyter Notebooks developed to perform the [fourth](http://home.agh.edu.pl/~mdig/dokuwiki/doku.php?id=teaching:courses:agh:weaiiib:inf:adv-ml:2018-19_l:labs:data_preparation_and_model_validation) assignment.